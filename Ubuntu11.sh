#!/usr/bin/env bash
wget https://sjc1-dsm.sdi.trendnet.org:4119/software/agent/SuSE_11/x86_64/ -O /tmp/agent.rpm --no-check-certificate --quiet
rpm -ihv /tmp/agent.rpm
sleep 15
/opt/ds_agent/dsa_control -r
/opt/ds_agent/dsa_control -a dsm://sjc1-dsm.sdi.trendnet.org:4120/
