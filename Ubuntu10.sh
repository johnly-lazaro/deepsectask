#!/usr/bin/env bash
wget https://sjc1-dsm.sdi.trendnet.org:4119/software/agent/Ubuntu_10.04/x86_64/ -O /tmp/agent.deb --no-check-certificate --quiet
dpkg -i /tmp/agent.deb
sleep 15
/opt/ds_agent/dsa_control -r
